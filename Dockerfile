FROM golang:1.14

WORKDIR /go/src/app
ADD . /go/src/app/

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
RUN cd /go/src/app;
RUN echo $GOPATH
RUN go get
RUN go build -o main;

EXPOSE 8080

ENTRYPOINT ["./main"]
