package users

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// UserShowHandler displays user
func UserShowHandler(c *gin.Context) {
	userName := c.Params.ByName("name")
	c.HTML(http.StatusOK, "users/show", gin.H{
		"name": userName,
	})
}

// UserIndexHandler displays users
func UserIndexHandler(c *gin.Context) {
	c.HTML(http.StatusOK, "users/index", gin.H{
		"now": time.Now(),
	})
}
