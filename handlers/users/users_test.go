package users_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/madhums/go-gin-mgo-demo/gin_html_render"
	"gitlab.com/michalsz/time_tracking/handlers/users"
)

func TestShowRegistrationPageUnauthenticated(t *testing.T) {
	r := getRouter(false)

	r.GET("/users", users.UserIndexHandler)

	req, _ := http.NewRequest("GET", "/users", nil)

	testHTTPResponse(t, r, req, func(w *httptest.ResponseRecorder) bool {
		statusOK := w.Code == http.StatusOK

		p, err := ioutil.ReadAll(w.Body)
		pageOK := err == nil && strings.Index(string(p), "<title>Gin demo</title>") > 0

		return statusOK && pageOK
	})
}
func testHTTPResponse(t *testing.T, r *gin.Engine, req *http.Request, f func(w *httptest.ResponseRecorder) bool) {

	// Create a response recorder
	w := httptest.NewRecorder()

	// Create the service and process the above request.
	r.ServeHTTP(w, req)

	if !f(w) {
		t.Fail()
	}
}

func getRouter(withTemplates bool) *gin.Engine {
	r := gin.Default()
	htmlRender := GinHTMLRender.New()
	htmlRender.TemplatesDir = "../../templates"
	htmlRender.Layout = "application"
	r.HTMLRender = htmlRender.Create()

	return r
}
