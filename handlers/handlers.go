package handlers

import (
	"errors"
	"time"

	"github.com/gin-gonic/gin"
)

// PingHandler return pong
func PingHandler(c *gin.Context) {
	c.Error(errors.New("this is an error"))
	c.String(200, "pong")
}

// NowHandler return pong
func NowHandler(c *gin.Context) {
	c.JSON(200, gin.H{"now": time.Now()})
}

//RedirectHandler it redirect to start page
func RedirectHandler(c *gin.Context) {
	c.Redirect(301, "/")
}
