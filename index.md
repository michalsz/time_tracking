# Documentation

## Build docker image

```sh
docker build --tag tt:latest .
```

## How to compile
```sh
go build -o main
```

## How to run app
```sh
./main
```