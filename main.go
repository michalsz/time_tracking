package main

import (
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/madhums/go-gin-mgo-demo/gin_html_render"
	"gitlab.com/michalsz/time_tracking/handlers"
	"gitlab.com/michalsz/time_tracking/handlers/users"
)

func main() {
	router := gin.Default()

	htmlRender := GinHTMLRender.New()
	htmlRender.Layout = "application"
	router.HTMLRender = htmlRender.Create()

	router.GET("/ping", handlers.PingHandler)
	router.GET("/now", handlers.NowHandler)
	router.GET("/redirect", handlers.RedirectHandler)
	router.GET("/", users.UserIndexHandler)
	router.GET("/users", users.UserIndexHandler)
	router.GET("/users/:name", users.UserShowHandler)

	router.Use(static.Serve("/public", static.LocalFile("./public", false)))
	router.Run(":8080")
}
