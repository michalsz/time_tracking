module gitlab.com/michalsz/time_tracking

go 1.14

require (
	github.com/gin-contrib/sse v0.1.0
	github.com/gin-contrib/static v0.0.0-20191128031702-f81c604d8ac2
	github.com/gin-gonic/gin v1.6.2
	github.com/golang/protobuf v1.3.5
	github.com/madhums/go-gin-mgo-demo v0.0.0-20150808235628-3c308faae11f
	github.com/mattn/go-isatty v0.0.12
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.9.0
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/ugorji/go v1.1.7
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa
	golang.org/x/tools v0.0.0-20200330040139-fa3cc9eebcfe // indirect
	gopkg.in/yaml.v2 v2.2.8
	github.com/go-playground/validator/v10 v10.2.0

)
